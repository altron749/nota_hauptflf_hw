local sensorInfo = {
	name = "ExampleDebug",
	desc = "Sends data to example debug widget",
	author = "PepeAmpere",
	date = "2018-04-16",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching
local THRESHOLD_DIST = 25


function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

local getUnitDefID = Spring.GetUnitDefID
local SpringGetUnitPosition = Spring.GetUnitPosition
local getHeight = Spring.GetGroundHeight
local HEI_THRESHOLD = 300
local DEFAULT_RANGE = 500


function isSafeFromTo(from, to)
local checked = false
local numberOfDangerousEnemies =0
local dangerous = false
local currentCheckPos = from
local vectorToGo = to - from
vectorToGo.y = 0
local normalized = vectorToGo:GetNormal()
while (not checked)
do
	if currentCheckPos:Distance(to)<100 then
		checked = true
	end
	currentCheckPos = currentCheckPos + normalized * 50 
	currentCheckPos.y = getHeight(currentCheckPos.x,currentCheckPos.z)
	local unitsInWay = Spring.GetUnitsInSphere(currentCheckPos.x,currentCheckPos.y,currentCheckPos.z,750)
	for j = 1, #unitsInWay do
		local defID = getUnitDefID(unitsInWay[j])	
		local unitX,unitY,unitZ = SpringGetUnitPosition(unitsInWay[j])
		local pathHei = getHeight(currentCheckPos.x,currentCheckPos.z)
		local inRange = false
		local visible = false
		local unitPos = Vec3(unitX,0,unitZ)
		local currpos = Vec3(currentCheckPos.x,0,currentCheckPos.z)
		local distance = unitPos:Distance(currpos)
		if Spring.GetUnitTeam(unitsInWay[j]) ~= Spring.GetLocalTeamID() and unitY - pathHei < HEI_THRESHOLD  then
			if(defID == nil)   then
				if distance < DEFAULT_RANGE then
					dangerous = true
					--numberOfDangerousEnemies = numberOfDangerousEnemies+1
				end
			else
				if UnitDefs[defID].maxWeaponRange>= distance then
					inRange = true
				end
				if UnitDefs[defID].losRadius >= distance then
					visible = true
				end
			end
			if visible and inRange then
				numberOfDangerousEnemies = numberOfDangerousEnemies+1
				dangerous = true
			end	

			
		end
	end
end 
return 
{
	dangerous = dangerous,
	numberOfDangerousEnemies = numberOfDangerousEnemies,
}
end



function addToQueue(que, toAdd)
	que[#que+1]=toAdd
	return
	{

	}
end

function pop(que)
	local tmp = que[1]
	table.remove(que,1)
	return{
	
	node = tmp
	
	}

end

return function(dangerMap, unitID, start)
	if unitID == nil then
		return{
			foundGoal = false,
			noNeedToLook = false,
			startNode=nil,
			number = 0,
			path = nil,
			enemiesEncountered = 0,
		}
	end
	local hei, wid = Game.mapSizeX, Game.mapSizeZ
	local res = dangerMap.res
	local posX,posY,posZ = SpringGetUnitPosition(unitID)
	local startNode = {}
	startNode["x"]=math.floor(start.x/res)
	startNode["y"]=math.floor(start.z/res)
	local goal = {}
	goal["x"]=math.floor(posX/res)
	goal["y"]=math.floor(posZ/res)
	local queue = {}
	addToQueue(queue, startNode)
	local foundGoal = false
	local lookedAt = {}
	local path = {}
	local goualNode
	local number = 0
	local noNeedToLook = false
	local enemiesEncountered = {}
if global.safePaths == nil then
	global.safePaths = {}
	local emptyPath = {}
	emptyPath["res"]=res
	addToQueue(emptyPath, startNode)
	global.safePaths[1] = emptyPath
end

for i=1,#global.safePaths do
	local currPath = global.safePaths[i]
	local endPointOfPath = Vec3(currPath[1].x* currPath.res + currPath.res/2,0,currPath[1].y* currPath.res + currPath.res/2)
	local endPosition = Vec3(posX, posY, posZ)
	local isSafe = isSafeFromTo(endPointOfPath,endPosition)
	if not isSafe.dangerous then
		path = currPath
		noNeedToLook = true
		break
	end
	enemiesEncountered[#enemiesEncountered+1] = isSafe.numberOfDangerousEnemies

end

	while #queue > 0 and not foundGoal and not noNeedToLook do
		number = number +1
		local currentNode = pop(queue).node
		local index = dangerMap.wid*(currentNode.x-1-1)+currentNode.y
		if(lookedAt[index]==nil and currentNode.x>1 and dangerMap[index] == 0 )then 
			local toAdd = {}
			toAdd["x"]=currentNode.x-1
			toAdd["y"] = currentNode.y
			toAdd["pred"] = currentNode
			addToQueue(queue,toAdd)
			lookedAt[index] = 1
		end 
		local index = dangerMap.wid*(currentNode.x-1)+currentNode.y-1
		
		if(lookedAt[index]==nil and currentNode.y>1 and dangerMap[index] == 0) then 
			local toAdd = {}
			toAdd["x"]=currentNode.x
			toAdd["y"] = currentNode.y-1
			toAdd["pred"] = currentNode			
			addToQueue(queue,toAdd)
			lookedAt[index] = 1
		end

		local index = dangerMap.wid*(currentNode.x)+currentNode.y
		if(lookedAt[index]==nil and currentNode.x<dangerMap.wid and dangerMap[index] == 0) then 
			local toAdd = {}
			toAdd["x"]=currentNode.x+1
			toAdd["y"] = currentNode.y
			toAdd["pred"] = currentNode			
			addToQueue(queue,toAdd)
			lookedAt[index] = 1
		end

		local index = dangerMap.wid*(currentNode.x-1)+currentNode.y+1
		if(lookedAt[index]==nil and currentNode.y<dangerMap.hei and dangerMap[index] == 0) then 

			local toAdd = {}
			toAdd["x"]=currentNode.x
			toAdd["y"] = currentNode.y+1
			toAdd["pred"] = currentNode			
			addToQueue(queue,toAdd)
			lookedAt[index] = 1
		end

		if currentNode.x == goal.x and currentNode.y== goal.y then
			foundGoal =true
			goualNode = currentNode
		end
	end
	if foundGoal then
		local currNode = goualNode
		local pathFinished = false
		while not pathFinished do
			addToQueue(path,currNode)
			if(currNode==startNode) then
				pathFinished = true
			end
			currNode = currNode.pred
		end
	path["res"] = res
	global.safePaths[#global.safePaths+1] = path
	end

	return{
		foundGoal = foundGoal,
		noNeedToLook = noNeedToLook,
		startNode=startNode,
		number = number,
		path = path,
		enemiesEncountered = enemiesEncountered,
		goal=goal,
		unitID=unitID,
	}
end