local sensorInfo = {
	name = "ExampleDebug",
	desc = "Sends data to example debug widget",
	author = "PepeAmpere",
	date = "2018-04-16",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching
local THRESHOLD_DIST = 25


function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

local getUnitDefID = Spring.GetUnitDefID
local SpringGetUnitPosition = Spring.GetUnitPosition
local getHeight = Spring.GetGroundHeight

return function(resolution, baseHeigth)
	-- Opens a file in append mode
	file = io.open("mapText.lua", "a")

	-- sets the default output file as test.lua
	io.output(file)
	local baseHeigth = baseHeigth
	local DEFAULT_RANGE = 1000
	local MIN_HEI_DIFF = 300
	local HEI_RES = 20
	local hei, wid = Game.mapSizeX, Game.mapSizeZ
	local res = resolution
	local teams = Spring.GetTeamList()
	for i=1,#teams do
		if(teams[i]==Spring.GetLocalTeamID()) or (teams[i] == Spring.GetGaiaTeamID) then
			table.remove(teams,i)
			i = i-1 
		end
	end
	local enemyTeamID=teams[1]
	local enemyUnits = Spring.GetTeamUnits(enemyTeamID)
	local dangerMap = {}
	local numberOfEnemyTiles = 0
	local debugNumber = 0
	dangerMap["res"] = resolution
	dangerMap["hei"] = math.floor(hei/res)
	dangerMap["wid"] = math.floor(wid/res)
	for i=1,dangerMap.hei do
		for j=1,dangerMap.wid do
			local inRange = false
			local visible = false
			local curX = (i-1)*res + res/2
			local curZ = (j-1)*res + res/2
			index = (i-1)*dangerMap.wid+j
			dangerMap[index] = 0
			local unitsInWay = Spring.GetUnitsInCylinder(curX,curZ,1500)
			for k = 1, #unitsInWay do
				local pos = Vec3(curX,0,curZ)
				local enemX, enemY, enemZ = SpringGetUnitPosition(unitsInWay[k])
				local enemPos = Vec3(enemX,0,enemZ)
				local distance = enemPos:Distance(pos) 
				if Spring.GetUnitTeam(unitsInWay[k]) ~= Spring.GetLocalTeamID() and (enemY - getHeight(curX,curZ))< MIN_HEI_DIFF then  --and 
					local defID = getUnitDefID(unitsInWay[k])	
					if(defID == nil)   then
						if distance < DEFAULT_RANGE then
							dangerMap[index] = 1
						end
					else
						if UnitDefs[defID].maxWeaponRange + res >= distance then
							inRange = true
						end
						if UnitDefs[defID].losRadius + res >= distance then
							visible = true
						end
					if visible and inRange then
						dangerMap[index] = 1
					end	
 
					end	
											
				end
				if getHeight(curX,curZ) > baseHeigth + HEI_RES then
					dangerMap[index] = 1
				end

			end
			debugNumber = index
		end
	end
	newDangerMap = {}
	newDangerMap["res"]=dangerMap.res
	newDangerMap["hei"]=dangerMap.hei
	newDangerMap["wid"]=dangerMap.wid
	local tmpI =0
	local tmpJ = 0
	for i=1,dangerMap.hei do
		for j=1,dangerMap.wid do
			newDangerMap[(j-1)*dangerMap.wid+i] = dangerMap[(j-1)*dangerMap.wid+i]
			if(j < dangerMap.wid) then
				tmpJ = 1
				tmpI = 0
				if dangerMap[(j+tmpJ-1)*dangerMap.wid+i+tmpI]~=0 then
					newDangerMap[(j-1)*dangerMap.wid+i] =1
				end
			end
			if(j>1) then
				tmpJ = -1
				tmpI = 0
				if dangerMap[(j+tmpJ-1)*dangerMap.wid+i+tmpI]~=0 then
					newDangerMap[(j-1)*dangerMap.wid+i] =1
				end
			end
			if i < dangerMap.hei then
				tmpJ = 0
				tmpI = 1
				if dangerMap[(j+tmpJ-1)*dangerMap.wid+i+tmpI]~=0 then
					newDangerMap[(j-1)*dangerMap.wid+i] = 1
				end
			end
			if( i > 1) then
				tmpJ = 0
				tmpI = -1
				if dangerMap[(j+tmpJ-1)*dangerMap.wid+i+tmpI]~=0 then
					newDangerMap[(j-1)*dangerMap.wid+i] = 1 
				end
			end
		end

	end


	for i=1,newDangerMap.hei do
		for j=1,newDangerMap.wid do
			io.write(newDangerMap[(j-1)*newDangerMap.wid+i])
			io.write(" ")
			if newDangerMap[(j-1)*newDangerMap.wid+i]==0 then
				numberOfEnemyTiles = numberOfEnemyTiles +1
			end
		end
		io.write('\n')
	end
	io.close(file)


	return{
	 	dangerMap=newDangerMap,
	 	wid = newDangerMap.wid,
	 	hei = newDangerMap.hei,
	numberOfEnemyTiles=numberOfEnemyTiles,
	numberOfTiles = newDangerMap.wid*newDangerMap.hei,
	debugNumber = debugNumber,
	}
end