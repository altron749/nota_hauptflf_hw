local sensorInfo = {
	name = "ExampleDebug",
	desc = "Sends data to example debug widget",
	author = "PepeAmpere",
	date = "2018-04-16",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching
local THRESHOLD_DIST = 25


function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end


local SpringGetUnitPosition = Spring.GetUnitPosition

return function(safeArea)
	local savePos = safeArea.center
	local radius = safeArea.radius
	local myUnits = Spring.GetTeamUnits(Spring.GetMyTeamID())
	local currentMin = 100000
	local currentClosestID
	local chosen = false
	local number=0;
	local numberOfEnemiesSpotted = 0
	local numberOfTimesGoneThroughPathWhile = 0;
	local unitIDs = {}
	local currentIndex = 1
	for i=1,#myUnits do
		local pointX, pointY, pointZ = SpringGetUnitPosition(myUnits[i])
		local unitPos = Vec3(pointX,pointY,pointZ)
		local dist = unitPos:Distance(savePos)
		local dangerous = false
		if  dist > radius then
			if not UnitDefs[Spring.GetUnitDefID(myUnits[i])].cantBeTransported then
				currentClosestID = myUnits[i]
				unitIDs[currentIndex] = myUnits[i]
				currentIndex = currentIndex +1
			end
		end
	end

	return{
	 	id = currentClosestID,
	 	dist = currentMin,
	 	number = number,
	 	numberOfEnemiesSpotted=numberOfEnemiesSpotted,
	 	numberOfTimesGoneThroughPathWhile =numberOfTimesGoneThroughPathWhile,
	 	numberOfUnits = #myUnits,
	 	unitIDs = unitIDs
	}
end