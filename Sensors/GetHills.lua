local sensorInfo = {
	name = "ExampleDebug",
	desc = "Sends data to example debug widget",
	author = "PepeAmpere",
	date = "2018-04-16",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

local getHeight = Spring.GetGroundHeight
local getExtremes = Spring.GetGroundExtremes


function getSquaredDistance(x1,y1,x2,y2)
	local dst = (x1-x2)*(x1-x2)+(y1-y2)*(y1-y2);
return dst

end


-- @description return current wind statistics
return function()
	local hillsX = {}
	local hillsY= {}
	local hillsWei = {}
	for j=1, 4, 1
	do
		hillsX[j]=0;
		hillsY[j]=0;
		hillsWei[j]=0;
	end
	local hei, wid = Game.mapSizeX, Game.mapSizeZ
	local minHei, maxHei = getExtremes()
	local currentHei
	local maxX = 0
	local maxY = 0
	local currentMax = 0
	local numberLoops = 0;
	for i = 1,hei, 50
	do
		for k = 1,wid, 50
		do 
			numberLoops = numberLoops +1;
			currentHei = getHeight(i,k);
			if  currentHei > (maxHei - 10)
			then
				for l=1,4 do
					local dist = getSquaredDistance(i,k,hillsX[l],hillsY[l])
					if dist <300000
					then
						hillsX[l]=(hillsX[l]*hillsWei[l] + i)/(hillsWei[l]+1);
						hillsY[l]=(hillsY[l]*hillsWei[l] + k)/(hillsWei[l]+1);
						hillsWei[l]=hillsWei[l]+1;
						break;

					elseif hillsWei[l]==0
					then
						hillsX[l]=i;
						hillsY[l]=k;
						hillsWei[l]=1;
						break;
					end
				end
				currentMax  = currentHei;
				maxX = i;
				maxY = k;
			end
		end 
	end

	return{
	currentMax = currentMax,
	hei = hei,
	wid = wid,
	numberLoops =numberLoops,
	maxHei = maxHei,
	minHei = minHei,
	hillsX=hillsX,
	hillsY=hillsY,

	}
end