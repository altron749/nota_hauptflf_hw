function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move to defined position",
		parameterDefs = {
			{ 
				name = "unitID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
						{ 
				name = "orderOfUnit", 
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "0",
			},
		}
	}
end

-- constants
local THRESHOLD_STEP = 25
local THRESHOLD_DEFAULT = 1000

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
	self.lastPointmanPosition = Vec3(0,0,0)
end

function Run(self, units, parameter)
	local unitID = parameter.unitID
	local pointX, pointY, pointZ = SpringGetUnitPosition(unitID)
	local position = Vec3(pointX,pointY,pointZ)
	local orderOfUnit = parameter.orderOfUnit 
	
	
	-- pick the spring command implementing the move
	local cmdID = CMD.MOVE

	local pointman = units[orderOfUnit] -- while this is running, we know that #units > 0, so pointman is valid
	local pointX, pointY, pointZ = SpringGetUnitPosition(pointman)
	local pointmanPosition = Vec3(pointX, pointY, pointZ)
	
	-- threshold of pointan success
	if (pointmanPosition == self.lastPointmanPosition) then 
		self.threshold = self.threshold + THRESHOLD_STEP 
	else
		self.threshold = THRESHOLD_DEFAULT
	end
	self.lastPointmanPosition = pointmanPosition
	
--	 check pointman success
--	 THIS LOGIC IS TEMPORARY, NOT CONSIDERING OTHER UNITS POSITION
	if (pointmanPosition:Distance(position) < self.threshold) then
		return SUCCESS
	else
		SpringGiveOrderToUnit(pointman, cmdID, position:AsSpringVector(), {})		
		return RUNNING
	end
end


function Reset(self)
	ClearState(self)
end
