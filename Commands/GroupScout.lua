function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move to defined position",
		parameterDefs = {
			{ 
				name = "relativePosition",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
						{ 
				name = "numberOfUnits",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "20",
			},
		}
	}
end

-- constants
local THRESHOLD_STEP = 25
local THRESHOLD_DEFAULT = 400

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
end

function Run(self, units, parameter)
	local position = Vec3(0,0,0) -- Vec3
	local hei, wid = Game.mapSizeX, Game.mapSizeZ
	local ended = false
	local numberOfScouts = parameter.numberOfUnits
	local sqrtNumberOfScouts = math.sqrt(numberOfScouts)
	local sqrtFloor = math.floor(sqrtNumberOfScouts)
	-- pick the spring command implementing the move
	local cmdID = CMD.MOVE

	local pointman = units[1] -- while this is running, we know that #units > 0, so pointman is valid
	local pointX, pointY, pointZ = SpringGetUnitPosition(pointman)
	local pointmanPosition = Vec3(pointX, pointY, pointZ)
	self.threshold = THRESHOLD_DEFAULT
	
	for i=1,#units do
		local x,y,z = SpringGetUnitPosition(units[i])
		local unitPosition = Vec3(x,0,z)
		ended = true
		mod = i%sqrtFloor
		div = math.floor(i/sqrtFloor)
		position.x = mod* (hei/sqrtNumberOfScouts) + 1/2 * (hei/sqrtNumberOfScouts)
		position.z = div * (wid/sqrtNumberOfScouts) + 1/2 * (wid/sqrtNumberOfScouts)
			SpringGiveOrderToUnit(units[i], cmdID, position:AsSpringVector(), {})	
		if position:Distance(unitPosition) > THRESHOLD_DEFAULT then
			ended = false
		end	
	end

	if ended then
		return SUCCESS
	end

		return RUNNING
end


function Reset(self)
	ClearState(self)
end
