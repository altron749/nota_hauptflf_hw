function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move to defined position",
		parameterDefs = {
			{ 
				name = "unitID",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "orderOfUnit", 
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "0",
			},

		}
	}
end

-- constants
local THRESHOLD_STEP = 25
local THRESHOLD_DEFAULT = 0

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetUnitIsTransporting = Spring.GetUnitIsTransporting


local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
	self.lastPointmanPosition = Vec3(0,0,0)
end

function Run(self, units, parameter)
	local unitID = parameter.unitID 
	local orderOfUnit = parameter.orderOfUnit 
	
	if #units < orderOfUnit or unitID == nil then
		return SUCCESS
	end
	

	
	-- pick the spring command implementing the move
	local cmdID = CMD.LOAD_UNITS
	SpringGiveOrderToUnit(units[orderOfUnit], cmdID, {unitID}, {})		
	if( #SpringGetUnitIsTransporting(units[orderOfUnit]) <1) then
		return RUNNING
	else
		return SUCCESS
	end
end


function Reset(self)
	ClearState(self)
end
