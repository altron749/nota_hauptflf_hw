function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move along path (in reverse order)",
		parameterDefs = {
			{ 
				name = "path",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "orderOfUnit", 
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "0",
			},
			{ 
				name = "fight",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "false",
			}
		}
	}
end

-- constants
local THRESHOLD_STEP = 25
local THRESHOLD_DEFAULT = 200

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local getHeight = Spring.GetGroundHeight

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
	self.lastPointmanPosition = Vec3(0,0,0)
end

function Run(self, units, parameter)

	local path = parameter.path -- Vec3
	local orderOfUnit = parameter.orderOfUnit 
	local fight = parameter.fight -- boolean
	if #units < orderOfUnit or path== nil then
		return SUCCESS
	end
	
	-- pick the spring command implementing the move
	local cmdID = CMD.MOVE
	if (fight) then cmdID = CMD.FIGHT end

	local pointman = units[orderOfUnit] -- while this is running, we know that #units > 0, so pointman is valid
	local pointX, pointY, pointZ = SpringGetUnitPosition(pointman)
	local pointmanPosition = Vec3(pointX, pointY, pointZ)
	
	-- threshold of pointan success
	if (pointmanPosition == self.lastPointmanPosition) then 
		self.threshold = self.threshold + THRESHOLD_STEP 
	else
		self.threshold = THRESHOLD_DEFAULT
	end
	self.lastPointmanPosition = pointmanPosition
	local pointmanPositionXZ = Vec3(pointmanPosition.x,0,pointmanPosition.z)
	
	-- check pointman success
	-- THIS LOGIC IS TEMPORARY, NOT CONSIDERING OTHER UNITS POSITION
	if (pointmanPositionXZ:Distance(Vec3((path[1].x-1) * path.res + path.res/2,0,(path[1].y-1) * path.res + path.res/2)) < self.threshold) then
			return SUCCESS
		
	else
		if #Spring.GetUnitCommands(pointman) < 1 then
			for i=0,#path-1 do
						local x = (path[#path-i].x-1) * path.res + path.res/2
						local y = (path[#path-i].y-1) * path.res + path.res/2

						local goal = Vec3(x,0,y)
						SpringGiveOrderToUnit(pointman, cmdID, goal:AsSpringVector(), {"shift"})
			end
		end
		return RUNNING
	end
end


function Reset(self)
	ClearState(self)
end
